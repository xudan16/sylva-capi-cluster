{{- define "XMachineTemplate-MD" }}
{{- $envAll := . }}
{{- range $machine_deployment_name, $machine_deployment_specs := .Values.machine_deployments }}

  {{/*********** Finalize the definition of the machine_deployments.X
  item by merging the machine_deployment_default */}}
  {{- $machine_deployment_def := dict -}}
  {{- $machine_deployment_def := deepCopy ($envAll.Values.machine_deployment_default | default dict) -}}
  {{- $machine_deployment_def := mergeOverwrite $machine_deployment_def $machine_deployment_specs -}}

  {{/*********** Prepare complete labels used in generated object */}}
  {{- $labels := dict -}}
  {{- $labels := deepCopy ($machine_deployment_def.metadata.labels | default dict) -}}
  {{- $_ := mergeOverwrite $labels (include "sylva-capi-cluster.labels" $envAll | fromYaml) -}}

  {{/*********** Set the default machine_deployments.X.infra_provider, if not specifically defined */}}
  {{ if not ($machine_deployment_def.infra_provider) }}
  {{- $_ := set $machine_deployment_def "infra_provider" $envAll.Values.capi_providers.infra_provider -}}
  {{ end }}

  {{/*********** Set the default machine_deployments.X.identity_ref_secret, if not specifically defined for CAPO MD */}}
  {{ if eq $machine_deployment_def.infra_provider "capo" }}
  {{ if not ($machine_deployment_def.identity_ref_secret) }}
  {{- $_ := set $machine_deployment_def "identity_ref_secret" (printf "%s-capo-cloud-config" $envAll.Values.name) -}}
  {{ end }}
  {{ end }}
---
{{ if eq $machine_deployment_def.infra_provider "capd" }}
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: DockerMachineTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ include "DockerMachineTemplateSpec" $envAll | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    role: worker
{{ $labels | toYaml | indent 4 }}
spec:
  template:
    spec:
{{ include "DockerMachineTemplateSpec" $envAll | indent 6 }}
{{ else if eq $machine_deployment_def.infra_provider "capo" }}
apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
kind: OpenStackMachineTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.identity_ref_secret $machine_deployment_def.network_interfaces | include "OpenStackMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    role: worker
{{ $labels | toYaml | indent 4 }}
spec:
  template:
    spec:
{{ tuple $envAll $machine_deployment_def.identity_ref_secret $machine_deployment_def.network_interfaces | include "OpenStackMachineTemplateSpec-MD" | indent 6 }}
{{ else if eq $machine_deployment_def.infra_provider "capv" }}
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: VSphereMachineTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.profile | include "VSphereMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    role: worker
{{ $labels | toYaml | indent 4 }}
spec:
  template:
    spec:
{{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.profile | include "VSphereMachineTemplateSpec-MD" | indent 6 }}
{{ else if eq $machine_deployment_def.infra_provider "capm3" }}
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: Metal3MachineTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.provisioning_pool_interface $machine_deployment_def.public_pool_interface $machine_deployment_def.capm3 | include "Metal3MachineTemplateSpec-MD" | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
  labels:
    role: worker
{{ $labels | toYaml | indent 4 }}
spec:
  template:
    spec:
{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.provisioning_pool_interface $machine_deployment_def.public_pool_interface $machine_deployment_def.capm3 | include "Metal3MachineTemplateSpec-MD" | indent 6 }}
---
{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.provisioning_pool_interface $machine_deployment_def.public_pool_interface $machine_deployment_def.capm3 | include "Metal3DataTemplate-MD" }}
{{ end }}
{{- end }}
{{- end }}
