{{- define "base-RKE2ConfigTemplateSpec" }}
{{- $envAll := index . 0 -}}
{{- $machine_kubelet_extra_args := index . 1 -}}
{{- $machine_rke2_additional_user_data := index . 2 -}}

agentConfig:
{{- if $envAll.Values.cis_profile }}
  cisProfile: {{ $envAll.Values.cis_profile }}
{{- end }}
  additionalUserData:
    config: |{{ mergeOverwrite (dict) ( $envAll.Values.default.rke2.additionalUserData.config | default "" | fromYaml) ( $machine_rke2_additional_user_data.config | fromYaml) | toYaml | nindent 6 }}
    strict: {{ pluck "strict" $envAll.Values.default.rke2.additionalUserData $machine_rke2_additional_user_data | last | default false | toYaml }}
  version: {{ $envAll.Values.k8s_version }}
  airGapped: {{ $envAll.Values.air_gapped }}
  kubelet:
    extraArgs:
      {{ range $kubelet_flag_key, $kubelet_flag_value := mergeOverwrite (deepCopy $envAll.Values.default.kubelet_extra_args) $machine_kubelet_extra_args }}
      - {{ printf "%s=%s" $kubelet_flag_key $kubelet_flag_value | quote }}
      {{ end }}
  {{- if $envAll.Values.ntp }}
  ntp:
{{ $envAll.Values.ntp | toYaml | indent 4 }} {{/* this line needs to have no leading spaces to ensure correct rendering */}}
  {{- end }}
preRKE2Commands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  {{- if and (not (eq $envAll.Values.capi_providers.infra_provider "capd")) $envAll.Values.dns_resolver }}
  - systemctl restart systemd-resolved
  {{- end }}
  {{- if $envAll.Values.proxies.http_proxy }}
  - export HTTP_PROXY={{ $envAll.Values.proxies.http_proxy }}
  - export HTTPS_PROXY={{ $envAll.Values.proxies.https_proxy }}
  - export NO_PROXY={{ $envAll.Values.proxies.no_proxy }}
  {{- end }}
files:
{{ $rke2ctfiles := list }}
{{ $rke2ctfiles = include "rke2_config_toml" $envAll | append $rke2ctfiles -}}
{{- if $envAll.Values.dns_resolver }}
    {{- $rke2ctfiles = include "resolv_conf" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- if ($envAll.Values.registry_mirrors | dig "hosts_config" "") }}
    {{- $rke2ctfiles = include "registry_mirrors" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- if $envAll.Values.proxies.http_proxy }}
    {{- $rke2ctfiles = include "rke2_agent_containerd_proxy" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- if $rke2ctfiles -}}
    {{- range $rke2ctfiles -}}
        {{ . | indent 2 }}
    {{- end }}
{{- else }}
        []
{{- end }}
{{- end }}
