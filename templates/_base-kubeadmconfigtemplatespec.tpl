{{- define "base-KubeadmConfigTemplateSpec" }}
{{- $envAll := index . 0 -}}
{{- $machine_kubelet_extra_args := index . 1 -}}
{{- $machine_pre_kubeadm_commands := index . 2 -}}

joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs: {{ mergeOverwrite (deepCopy $envAll.Values.default.kubelet_extra_args) $machine_kubelet_extra_args | toYaml | nindent 6 }}
ntp: {{ $envAll.Values.ntp | toYaml | nindent 2 }}
preKubeadmCommands:
  {{ $envAll.Values.default.kubeadm.pre_kubeadm_commands | default "" | toYaml | nindent 2 }}
  {{ $machine_pre_kubeadm_commands | toYaml | nindent 2 }}
  {{- if and (not (eq $envAll.Values.capi_providers.infra_provider "capd")) $envAll.Values.dns_resolver }}
  - systemctl restart systemd-resolved
  {{- end }}
  {{- if $envAll.Values.proxies.http_proxy }}
  - systemctl daemon-reload
  - systemctl restart containerd.service
  {{- end }}
  - echo "Preparing Kubeadm bootstrap" > /var/log/my-custom-file.log
files:
{{- $kubeadmctfiles := list -}}
{{- if $envAll.Values.dns_resolver -}}
    {{- $kubeadmctfiles = include "resolv_conf" $envAll | append $kubeadmctfiles -}}
{{- end }}
{{- if ($envAll.Values.registry_mirrors | dig "hosts_config" "") -}}
    {{-  $kubeadmctfiles = include "registry_mirrors" $envAll | append $kubeadmctfiles -}}
{{- end }}
{{- if $envAll.Values.proxies.http_proxy }}
    {{-  $kubeadmctfiles = include "containerd_proxy_conf" $envAll | append $kubeadmctfiles -}}
{{- end }}
{{- if $kubeadmctfiles }}
    {{- range $kubeadmctfiles -}}
        {{ . | indent 2 }}
    {{- end }}
{{- else }}
        []
{{- end }}
{{- end }}
