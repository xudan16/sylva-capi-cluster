{{- define "base-RKE2ControlPlaneSpec" }}

registrationMethod: address
registrationAddress: {{ .Values.cluster_external_ip }}
agentConfig:
{{- if .Values.cis_profile }}
  cisProfile: {{ .Values.cis_profile }}
{{- end }}
  additionalUserData:
    config: |{{ mergeOverwrite (dict) ( .Values.default.rke2.additionalUserData.config | default "" | fromYaml) ( .Values.control_plane.rke2.additionalUserData.config | default "" | fromYaml) | toYaml | nindent 6 }}
    strict: {{ pluck "strict" .Values.default.rke2.additionalUserData .Values.control_plane.rke2.additionalUserData | last | default false | toYaml }}
  version: {{ .Values.k8s_version }}
  airGapped: {{ .Values.air_gapped }}
  nodeTaints: []
  kubelet:
    extraArgs:
      {{ range $kubelet_flag_key, $kubelet_flag_value := mergeOverwrite (deepCopy .Values.default.kubelet_extra_args) (deepCopy .Values.control_plane.kubelet_extra_args) }}
      - {{ printf "%s=%s" $kubelet_flag_key $kubelet_flag_value | quote }}
      {{ end }}
  {{- if .Values.ntp }}
  ntp:
{{ .Values.ntp | toYaml | indent 4 }} {{/* this line needs to have no leading spaces to ensure correct rendering */}}
  {{- end }}
serverConfig:
  cni: {{ .Values.default_cni }}
preRKE2Commands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  {{- if and (not (eq .Values.capi_providers.infra_provider "capd")) .Values.dns_resolver }}
  - systemctl restart systemd-resolved
  {{- end }}
  {{- if .Values.proxies.http_proxy }}
  - export HTTP_PROXY={{ .Values.proxies.http_proxy }}
  - export HTTPS_PROXY={{ .Values.proxies.https_proxy }}
  - export NO_PROXY={{ .Values.proxies.no_proxy }}
  {{- end }}
  - echo 'alias ctr="/var/lib/rancher/rke2/bin/ctr --namespace k8s.io --address /run/k3s/containerd/containerd.sock"' >> /root/.bashrc
  - echo 'alias crictl="/var/lib/rancher/rke2/bin/crictl --runtime-endpoint /run/k3s/containerd/containerd.sock"' >> /root/.bashrc
  - echo 'alias kubectl="KUBECONFIG=/etc/rancher/rke2/rke2.yaml /var/lib/rancher/rke2/bin/kubectl"' >> /root/.bashrc
files:
{{ $rke2cpfiles := list }}
{{- if .Values.cis_profile }}
    {{ $rke2cpfiles = include "kubernetes_rke2_cert_manager_psp" . | append $rke2cpfiles }}
    {{- if eq .Values.cis_profile "cis-1.6" }}
        {{ $rke2cpfiles = include "kubernetes_rke2_metallb_psp" . | append $rke2cpfiles }}
        {{ $rke2cpfiles = include "kubernetes_rke2_tigera_psp" . | append $rke2cpfiles  }}
    {{- end }}
{{- end }}
{{- if .Values.dns_resolver }}
    {{- $rke2cpfiles = include "resolv_conf" . | append $rke2cpfiles -}}
{{- end }}
{{- if (.Values.registry_mirrors | dig "hosts_config" "") }}
    {{- $rke2cpfiles = include "registry_mirrors" . | append $rke2cpfiles  -}}
{{- end }}
{{- if .Values.proxies.http_proxy }}
    {{- $rke2cpfiles = include "rke2_server_containerd_proxy" . | append $rke2cpfiles -}}
    {{- $rke2cpfiles = include "rke2_agent_containerd_proxy" . | append $rke2cpfiles -}}
{{- end }}
{{- if or (eq .Values.capi_providers.infra_provider "capo") (eq .Values.capi_providers.infra_provider "capv") (eq .Values.capi_providers.infra_provider "capm3") }}
    {{- $rke2cpfiles = include "kubernetes_rke2_ingress" . | append $rke2cpfiles  -}}
    {{- $rke2cpfiles = include "kubernetes_rke2_metallb" . | append $rke2cpfiles  -}}
    {{- $rke2cpfiles = include "kubernetes_rke2_metallb_l3" . | append $rke2cpfiles  -}}
    {{- $rke2cpfiles = include "kubernetes_rke2_vip" . | append $rke2cpfiles  -}}
    {{- $rke2cpfiles = include "rke2_config_toml" . | append $rke2cpfiles  -}}
    {{- $rke2cpfiles = include (printf "rke2_%s_helm_chart_config" .Values.default_cni) . | append $rke2cpfiles -}}
    {{- $rke2cpfiles = include "rke2_coredns_helm_chart_config" . | append $rke2cpfiles -}}
{{- end }}
{{- if $rke2cpfiles -}}
{{- range $rke2cpfiles }}
{{ . | indent 2 }} {{/* this line needs to have no leading spaces to ensure correct rendering */}}
{{- end }}
{{- else }}
    []
{{- end }}
{{- end }}
