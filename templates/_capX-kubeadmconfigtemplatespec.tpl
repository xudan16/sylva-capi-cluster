{{- define "capd-KubeadmConfigTemplateSpec" }}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      # Source: https://github.com/kubernetes-sigs/cluster-api/blob/main/test/infrastructure/docker/examples/simple-cluster.yaml
      eviction-hard: nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%
    name: {{`'{{ ds.meta_data.name }}'`}}
ntp: {}
preKubeadmCommands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  # Remove default mirroring configuration for k8s.gcr.io as it can't coexist with registry config dir
  - sed -i '/k8s.gcr.io/d' /etc/containerd/config.toml
  - grep -q config_path /etc/containerd/config.toml || echo "[plugins.\"io.containerd.grpc.v1.cri\".registry]\n  config_path = \"/etc/containerd/registry.d\"" >>  /etc/containerd/config.toml
files: []
{{- end }}

{{- define "capo-KubeadmConfigTemplateSpec" }}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: 'openstack:///{{`{{ ds.meta_data.uuid }}`}}'
    name: {{`'{{ ds.meta_data.name }}'`}}
ntp: {}
preKubeadmCommands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  # Remove default mirroring configuration for k8s.gcr.io as it can't coexist with registry config dir
  - sed -i '/k8s.gcr.io/d' /etc/containerd/config.toml
  - grep -q config_path /etc/containerd/config.toml || echo "[plugins.\"io.containerd.grpc.v1.cri\".registry]\n  config_path = \"/etc/containerd/registry.d\"" >>  /etc/containerd/config.toml
files: []
{{- end }}

{{- define "capv-KubeadmConfigTemplateSpec" }}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      cloud-provider: external
    name: {{`'{{ ds.meta_data.hostname }}'`}}
    criSocket: /var/run/containerd/containerd.sock
ntp: {}
preKubeadmCommands:
  - hostname {{`"{{ ds.meta_data.hostname }}"`}}
  - echo "::1         ipv6-localhost ipv6-loopback" >/etc/hosts
  - echo "127.0.0.1   localhost" >>/etc/hosts
  - echo "127.0.0.1   {{`{{ ds.meta_data.hostname }}`}}" >>/etc/hosts
  - echo {{`"{{ ds.meta_data.hostname }}"`}} >/etc/hostname
  - update-ca-certificates
files: []
{{- end }}

{{- define "capm3-KubeadmConfigTemplateSpec" }}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: metal3:///{{`{{ ds.meta_data.providerid }}`}}
    name: {{`'{{ ds.meta_data.name }}'`}}
ntp: {}
preKubeadmCommands:
  # dummy; added to cover for the case of empty list of commands
  - hostname {{`"{{ ds.meta_data.hostname }}"`}}
files: []
{{- end }}
